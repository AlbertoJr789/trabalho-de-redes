/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package util.ftp;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.swing.JOptionPane;
import util.ftp.modelo.Arquivo;
import util.ftp.modelo.Cabecalho;
import util.ftp.modelo.Pacote;

/**
 *
 * @author danie
 */
public class Commun {

    private static final byte[] buffer = new byte[32768];
    byte[] bytesLidos = null;
    private static FileInputStream fis = null;
    private static ByteArrayOutputStream baos;

    public static Arquivo encodeFileArquivo(File file) {
        Arquivo arquivo = new Arquivo();
        try {
            fis = new FileInputStream(file);
            arquivo.setCabecalho(new Cabecalho(file.getName(), (int) file.length()));
            int count;
            Pacote pacote;
            //Reading
            while ((count = fis.read(buffer)) > 0) {
                baos = new ByteArrayOutputStream();
                baos.write(buffer, 0, count);
                pacote = new Pacote();
                pacote.setPacote(baos.toByteArray());
                pacote.setSize(baos.size());
                baos.close();
                arquivo.getPacotes().add(pacote);
            }
            arquivo.getCabecalho().setSizePacotes(arquivo.getPacotes().size());
            fis.close();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "O Arquivo escolhido é inválido ! \nVerifique se não é apenas uma pasta\n" + e.getMessage());
            System.err.println("Erro ao quebrar arquivo em partes : " + e.getMessage());
        }
        return arquivo;
    }

}
