package util.ftp;

import java.awt.HeadlessException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import static java.util.Objects.isNull;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;
import util.ftp.modelo.Arquivo;
import util.ftp.modelo.Cabecalho;
import util.ftp.modelo.Pacote;
import util.ftp.modelo.Requisicao;
import util.ftp.modelo.Resposta;

public class Servidor {

    private JTextArea console;
    private int porta;
    ServerSocket server = null;
    Socket client = null;
    boolean roda = true;
    private ClientThread ct;
    private JProgressBar barra;

    public Servidor(JTextArea console, int porta, JProgressBar barra) {
        this.console = console;
        this.porta = porta;
        this.barra = barra;
        this.barra.setValue(0);

    }

    public void doConnections() {

        try {
            server = new ServerSocket(this.porta);
            this.console.append("Servidor ouvindo a porta " + this.porta);
            while (roda) {
                System.out.println("nova thread");
                client = server.accept();
                ct = new ClientThread(client, this.console, this.barra);
                ct.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void closeConnections() {
        roda = false;
        try {
            if (!isNull(client)) {
                client.shutdownInput();
                client.shutdownOutput();
                client.close();
            }
            server.close();
            this.console.append("\nConexão com o servidor encerrada\n");
        } catch (Exception e) {
            this.console.append("Erro ao fechar servidor: " + e.getMessage() + "\n");
            e.printStackTrace();
        }
    }
}

class ClientThread extends Thread {

    public JTextArea console;
    public JProgressBar barra;
    public Socket client = null;
    public ObjectInputStream in = null;
    public ObjectOutputStream out = null;
    public FileInputStream fis = null;
    public FileOutputStream fos = null;
    public File file = null;
    public Requisicao req;
    public Resposta res;
    public Arquivo arquivo;
    public Pacote pacote;
    private String IPCliente;

    public ClientThread(Socket c, JTextArea console, JProgressBar barra) {

        try {
            this.console = console;
            this.barra = barra;
            client = c;
            in = new ObjectInputStream(c.getInputStream());
            out = new ObjectOutputStream(c.getOutputStream());
            InetSocketAddress socketAddr = (InetSocketAddress) c.getRemoteSocketAddress();
            this.IPCliente = socketAddr.getAddress().getHostAddress();

        } catch (IOException e) {
            this.console.append("Erro cliente : " + e.getMessage() + "\n");
        }
    }

    private void Upload() {
        try {
            arquivo = new Arquivo();
            arquivo.setCabecalho((Cabecalho) req.getMessageContent());
            this.console.append(" - Recebendo novo arquivo: " + arquivo.getCabecalho().getNome() + "\n");
            res = new Resposta();
            res.setResponseCode(Resposta.REQUIRED_SEND_PACKAGE);
            out.writeObject(res);
            do {
                req = (Requisicao) in.readObject();
                if (req.getMessageType() == Requisicao.SEND_PACKAGE) {
                    arquivo.getPacotes().add((Pacote) req.getMessageContent());
                    res.setResponseCode(Resposta.REQUIRED_SEND_PACKAGE);
                    out.writeObject(res);
                    barra.setValue(arquivo.getPacotes().size() * 100 / arquivo.getCabecalho().getSizePacotes());
                } else if (req.getMessageType() == Requisicao.END_OF_FILE) {
                    this.console.append("Upload concluído " + arquivo.getCabecalho().getNome() + "\n");
                    File dir = new File("C:/Arquivos_FTP");
                    if (!dir.exists()) {
                        dir.mkdir();
                    }
                    fos = new FileOutputStream("C:/Arquivos_FTP/" + arquivo.getCabecalho().getNome());
                    this.console.append("Gravando arquivo em disco!\n");
                    for (Pacote pac : arquivo.getPacotes()) {
                        fos.write(pac.getPacote());
                    }
                }
            } while (req.getMessageType() == Requisicao.SEND_PACKAGE);
            fos.flush();
            fos.close();
            this.console.append("Arquivo gravado com sucesso!\n");
        } catch (HeadlessException e) {
            this.console.append("\n[HeadlessException]Erro ao receber arquivo: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            this.console.append("\n[IOException]Erro ao receber arquivo: " + e.getMessage());
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            this.console.append("[ClassNotFound]Erro ao receber arquivo: " + e.getMessage());
            e.printStackTrace();
        }

    }

    private void Download() throws ClassNotFoundException {
        try {
            Cabecalho c = (Cabecalho) req.getMessageContent();
            this.console.append(" - Fazendo download de arquivo: " + c.getNome() + "\n");
            File arquivoFile = new File("C:/Arquivos_FTP/" + c.getNome());
            res = new Resposta();
            if (arquivoFile.exists()) {
                arquivo = Commun.encodeFileArquivo(arquivoFile);
                res.setResponseCode(Resposta.FILE_EXISTS);
                res.setResponseContent(arquivo.getCabecalho());
                out.writeObject(res);
                int indice = 0;
                while (indice <= arquivo.getPacotes().size()) {
                    req = (Requisicao) in.readObject();
                    if (req.getMessageType() == Requisicao.DOWNLOAD_PACKAGE) {
                        res = new Resposta();
                        if (indice < arquivo.getPacotes().size()) {
                            res.setResponseCode(Resposta.SEND_PACKAGE);
                            res.setResponseContent(arquivo.getPacotes().get(indice));
                            indice++;
                            barra.setValue(indice * 100 / arquivo.getPacotes().size());
                        } else {
                            this.console.append("Arquivo enviado ao cliente\n");
                            indice++;
                            res.setResponseCode(Resposta.END_OF_FILE);
                        }
                        out.writeObject(res);
                    }
                }
            } else {
                this.console.append("Arquivo requisitado pelo cliente não foi encontrado\n");
                res.setResponseCode(Resposta.FILE_NOT_FOUND);
                out.writeObject(res);
            }

        } catch (HeadlessException e) {
            this.console.append("\n[HeadlessException]Erro ao enviar arquivo ao cliente: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            this.console.append("\n[IOException]Erro ao enviar arquivo ao cliente: " + e.getMessage());
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            this.console.append("\n[ClassNotFound]Erro ao enviar arquivo ao cliente: " + e.getMessage());
            e.printStackTrace();
        }

    }

    private void mandarNomes() {
        try {
            this.console.append(" - Requisitando o nome dos arquivos.");
            File pasta = new File("C:/Arquivos_FTP");
            res = new Resposta();
            List<String> lista = new ArrayList<String>();
            if (pasta.exists()) {
                File[] arquivos = pasta.listFiles();
                for (File arquivo : arquivos) {
                    if (arquivo.exists()) {
                        lista.add(arquivo.getName());
                    }
                }
                res.setResponseContent(lista);
                res.setResponseCode(Resposta.FILE_EXISTS);
                if (lista.isEmpty()) {
                   this.console.append("Servidor não possui nenhum arquivo no momento, notificando ao cliente...\n");
                } else {
                    this.console.append("Servidor enviando nome(s) do(s) arquivo(s) ao cliente...\n");
                }
            } else {
                this.console.append("Servidor não possui nenhum arquivo no momento, notificando ao cliente...\n");
                res.setResponseContent(lista);
                res.setResponseCode(Resposta.FILE_NOT_FOUND);
            }
            out.writeObject(res);
        } catch (HeadlessException e) {
            this.console.append("\n[HeadlessException]Erro ao enviar lista ao cliente: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            this.console.append("\n[IOException]Erro ao enviar lista ao cliente: " + e.getMessage());
            e.printStackTrace();
        }

    }

    @Override
    public void run() {
        try {
            req = (Requisicao) in.readObject();
            this.console.append("\nCliente " + this.IPCliente + "\n");
            if (req.getMessageType() == Requisicao.FILE_SENT_FROM_CLIENT) {
                Upload();
            } else if (req.getMessageType() == Requisicao.DOWNLOAD_FILE) {
                Download();
            } else if (req.getMessageType() == Requisicao.SHOW_FILES) {
                mandarNomes();
            }
        } catch (IOException | ClassNotFoundException e) {
            this.console.append(" - Erro ao processar requisição: " + e.getMessage() + "\n");
            e.printStackTrace();
        }
    }
}
