package util.ftp;

import java.awt.HeadlessException;
import java.util.List;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import util.ftp.Commun;
import util.ftp.modelo.Arquivo;
import util.ftp.modelo.Cabecalho;
import util.ftp.modelo.Pacote;
import util.ftp.modelo.Requisicao;
import util.ftp.modelo.Resposta;

public class Cliente {

    private Socket client = null;
    private ObjectInputStream in = null;
    private ObjectOutputStream out = null;
    private FileInputStream fis = null;
    private FileOutputStream fos = null;
    private int porta;
    private String host;
    private String filepath;
    private JProgressBar barra;
    private Requisicao req;
    private Resposta res;
    private Arquivo a;

    public Cliente(int porta, String host, String filepath, JProgressBar barra) {
        this.porta = porta;
        this.host = host;
        this.filepath = filepath;
        this.barra = barra;
        this.barra.setValue(0);
    }

    public void doConnections() {
        try {
            client = new Socket(this.host, this.porta);
            out = new ObjectOutputStream(client.getOutputStream());
            in = new ObjectInputStream(client.getInputStream());
            JOptionPane.showMessageDialog(null, "Conexão iniciada host : " + host + " , porta " + porta);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao iniciar conexão: " + e.getMessage() + "\nVerifique a conexão com o servidor !");
        }
    }

    public void enviarArquivo() {
        try {
            File file = new File(this.filepath);
            Requisicao req = new Requisicao();
            if (file.isFile()) {
                Arquivo a = Commun.encodeFileArquivo(file);
                req.setMessageType(Requisicao.FILE_SENT_FROM_CLIENT);
                req.setMessageContent(a.getCabecalho());
                out.writeObject(req);//escreve tipo de solicitação com o cabeçalho                
                int indice = 0;
                while (indice <= a.getPacotes().size()) {
                    Resposta res = (Resposta) in.readObject();
                    if (res.getResponseCode() == Resposta.REQUIRED_SEND_PACKAGE) {
                        req = new Requisicao();
                        if (indice < a.getPacotes().size()) {
                            req.setMessageType(Requisicao.SEND_PACKAGE);
                            req.setMessageContent(a.getPacotes().get(indice));
                            indice++;
                            barra.setValue(indice * 100 / a.getPacotes().size());
                        } else {
                            req.setMessageType(Requisicao.END_OF_FILE);
                            indice++;
                        }
                        out.writeObject(req);
                    }
                }
                JOptionPane.showMessageDialog(null, "Arquivo enviado com sucesso");
            } else {
                JOptionPane.showMessageDialog(null, "Erro: " + file.getAbsoluteFile() + " Arquivo não enviado!");
            }
            out.close();
            in.close();
            client.close();
        } catch (HeadlessException e) {
            JOptionPane.showMessageDialog(null, "[HeadlessException]Erro ao enviar arquivo: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "[IOException]Erro ao enviar arquivo: " + e.getMessage());
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, "[ClassNotFound]Erro ao enviar arquivo: " + e.getMessage());
            e.printStackTrace();
        }

    }

    public void baixarArquivo() {
        try {
            Cabecalho c = new Cabecalho(filepath, 0);
            req = new Requisicao();
            req.setMessageType(Requisicao.DOWNLOAD_FILE);
            req.setMessageContent(c);
            out.writeObject(req);
            res = (Resposta) in.readObject();
            if (res.getResponseCode() == Resposta.FILE_EXISTS) {
                a = new Arquivo();
                a.setCabecalho((Cabecalho) res.getResponseContent());
                req = new Requisicao();
                req.setMessageType(Requisicao.DOWNLOAD_PACKAGE);
                do {
                    out.writeObject(req); //pede outro Download
                    res = (Resposta) in.readObject();
                    if (res.getResponseCode() == Resposta.SEND_PACKAGE) {
                        a.getPacotes().add((Pacote) res.getResponseContent());
                        barra.setValue(a.getPacotes().size() * 100 / a.getCabecalho().getSizePacotes());
                    } else if (res.getResponseCode() == Resposta.END_OF_FILE) {
                        JFrame framePai = new JFrame();
                        JFileChooser salvar = new JFileChooser();
                        salvar.setDialogTitle("Salvar arquivo");
                        salvar.setSelectedFile(new File(a.getCabecalho().getNome()));
                        if (salvar.showSaveDialog(framePai) == JFileChooser.APPROVE_OPTION) {
                            File novoArquivo = salvar.getSelectedFile();
                            fos = new FileOutputStream(novoArquivo);
                            for (Pacote pac : a.getPacotes()) {
                                fos.write(pac.getPacote());
                            }
                            fos.flush();
                            fos.close();
                            JOptionPane.showMessageDialog(null, "Arquivo baixado com sucesso !");
                        }
                    }
                } while (res.getResponseCode() == Resposta.SEND_PACKAGE);
            } else if (res.getResponseCode() == Resposta.FILE_NOT_FOUND) {
                JOptionPane.showMessageDialog(null, "Arquivo não encontrado");
            }
            this.out.close();
            in.close();
            client.close();
        } catch (HeadlessException e) {
            JOptionPane.showMessageDialog(null, "[HeadlessException]Erro ao baixar arquivo: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "[IOException]Erro ao baixar arquivo: " + e.getMessage());
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, "[ClassNotFound]Erro ao baixar arquivo: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public DefaultListModel<String> pedirArquivos() {
        try {
            req = new Requisicao();
            req.setMessageType(Requisicao.SHOW_FILES);
            out.writeObject(req);
            res = (Resposta) in.readObject();
            if (res.getResponseCode() == Resposta.FILE_EXISTS) {
                DefaultListModel<String> listaCliente = new DefaultListModel<String>();
                ((List<String>) res.getResponseContent()).forEach(nome -> listaCliente.addElement(nome));
                return listaCliente;
            }
            out.close();
            in.close();
            client.close();
        } catch (HeadlessException e) {
            JOptionPane.showMessageDialog(null, "[HeadlessException]Erro ao receber lista de arquivos: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "[IOException]Erro ao receber lista de arquivos: " + e.getMessage());
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, "[ClassNotFound]Erro ao receber lista de arquivos: " + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

}
