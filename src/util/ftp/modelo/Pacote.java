/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package util.ftp.modelo;

/**
 *
 * @author danie
 */
public class Pacote implements java.io.Serializable {

    private byte[] pacote;
    private int size;

    public byte[] getPacote() {
        return pacote;
    }

    public void setPacote(byte[] pacote) {
        this.pacote = pacote;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
