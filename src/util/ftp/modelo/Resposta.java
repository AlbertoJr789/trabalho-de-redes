/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package util.ftp.modelo;

/**
 *
 * @author danie
 */
public class Resposta implements java.io.Serializable {
    
    public static int FILE_EXISTS = 1;
    public static int FILE_NOT_FOUND = 2;
    public static int CONTENT_AVALIABLE = 3;
    public static int END_OF_FILE = 4;
    public static int REQUIRED_SEND_HEADER = 5;
    public static int REQUIRED_SEND_PACKAGE = 6;
    public static int FINISHED_DOWNLOADING_FILE = 7;
    public static int SEND_PACKAGE = 8;
    public static int FINISH_SENDING_PACKAGE =9 ;
    public static int SEND_HEADER = 8;

    private int responseCode;
    private Object responseContent;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public Object getResponseContent() {
        return responseContent;
    }

    public void setResponseContent(Object responseContent) {
        this.responseContent = responseContent;
    }

}
