/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package util.ftp.modelo;

/**
 *
 * @author danie
 */
public class Cabecalho implements java.io.Serializable {

    private String nome;
    private int sizeBytes;
    private int sizePacotes;

    public Cabecalho(String nome, int sizeBytes) {
        this.nome = nome;
        this.sizeBytes = sizeBytes;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getSizeBytes() {
        return sizeBytes;
    }

    public void setSizeBytes(int sizeBytes) {
        this.sizeBytes = sizeBytes;
    }

    public int getSizePacotes() {
        return sizePacotes;
    }

    public void setSizePacotes(int sizePacotes) {
        this.sizePacotes = sizePacotes;
    }

}
