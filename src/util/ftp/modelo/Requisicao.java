/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package util.ftp.modelo;

/**
 *
 * @author danie
 */
public class Requisicao implements java.io.Serializable {
    
    public static int FILE_SENT_FROM_CLIENT = 0;
    public static int DOWNLOAD_FILE = 1;
    public static int SHOW_FILES = 2;
    public static int SEND_PACKAGE = 3;
    public static int DOWNLOAD_PACKAGE = 5;
    public static int END_OF_FILE = 6;

    private int messageType;
    private Object messageContent;

    public int getMessageType() {
        return messageType;
    }

    public void setMessageType(int mesageType) {
        this.messageType = mesageType;
    }

    public Object getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(Object messageContent) {
        this.messageContent = messageContent;
    }

}
